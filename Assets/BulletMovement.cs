using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    Vector3 pos_Bullet;
    float timeShot = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        pos_Bullet = transform.position;
        timeShot += Time.deltaTime;
        if (timeShot > 0.1)
        {
            gameObject.transform.position = new Vector3(pos_Bullet.x, pos_Bullet.y, pos_Bullet.z + 1.5F);
            timeShot = 0;
        }
        if (pos_Bullet.z >= 50)
        {
            Destroy(gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }
}
