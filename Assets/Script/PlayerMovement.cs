using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody rb;
    ///private Rigidbody rb_Bullet;
    [SerializeField] Vector3 MoveDirection;
    [SerializeField] public float Force = 2f;
    [SerializeField] public float JumpSpeed;
    [SerializeField] public float HorizontalSpeed;
    [SerializeField] public float VerticalSpeed;
    [SerializeField] public float timeLastSinceShot;
    [SerializeField] public GameObject bullet;
    [SerializeField] private Vector3 pos_Bullet;
    //[SerializeField] public Game;
    private bool IsGrounded = false;
    private bool IsJump = true;
    float time = 0.0f;
    [SerializeField] public HealthBar healthBar;
    float maxhealth = 100f;

    private void OnEnable()
    {
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //rb_Bullet = GetComponent<Rigidbody>();
        healthBar.SetMaxHealth(maxhealth);
        timeLastSinceShot = 0;
    }

    // Update is called once per frame
    void Update()
    {

        time += Time.deltaTime;
        timeLastSinceShot += Time.deltaTime;
        if (timeLastSinceShot >= 0.2f)
        {
            if (Input.GetKey(KeyCode.X))
            {
                Shoot();

            }
            timeLastSinceShot = 0.0f;
        }
        if (time >= 1.0f)
        {

            healthBar.SetHealth(maxhealth / 100f);
            time = 0.0f;
        }
        HorizontalSpeed = Input.GetAxis("Horizontal");
        VerticalSpeed = Input.GetAxis("Vertical");
        if (IsGrounded)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                if (IsJump)
                {
                    JumpSpeed = 2.0f;

                    IsJump = false;
                }
            }

            if (transform.position.y > 2)
            {
                JumpSpeed = 0.0f;
            }
            MoveDirection = new Vector3(HorizontalSpeed, JumpSpeed, VerticalSpeed);
            Movement(MoveDirection);
        }
        else
        {
            //rb.s
        }

        pos_Bullet = gameObject.transform.position;
    }
    private void FixedUpdate()
    {

    }
    void Movement(Vector3 dir)
    {
        if (rb)
        {

        }
        rb.AddForce(MoveDirection * Force);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Item")
        {
            healthBar.InCreaseHealth();
            Destroy(collision.gameObject);
        }
        if (collision != null)
        {
            IsGrounded = true;
            IsJump = true;
        }

    }
    public void GetVerticlePos()
    {
    }
    private void SetVerticlePos()
    {

    }
    private void OnMouseDown()
    {
        Debug.Log("abc");
        Debug.Log(pos_Bullet);
    }
    private void Shoot()
    {
        InstanceButtets();
        //bullet.transform.position += pos_Bullet * 10.0f;
    }
    private void InstanceButtets()
    {
        Instantiate(bullet, pos_Bullet, Quaternion.Euler(pos_Bullet.x, 90, 90)).name = "Bullet";
    }
}
