using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] public GameObject ItemPrefab;
    private GameObject[] allItem = null;
    private Vector3 MinRandPos = new Vector3 (-45.0f, 0, -45.1f);
    private Vector3 MaxRandPos = new Vector3 (45.0f, 0, 45.0f);
    public float SpeedRotate = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        int index = 1;
        for (int i = 1; i <= 5; i++)
        {
            Vector3 randSpawn = new Vector3(
                Random.Range(MinRandPos.x, MaxRandPos.x),
                3.5f,
                Random.Range(MinRandPos.z, MaxRandPos.z));
         Instantiate(ItemPrefab, randSpawn, Quaternion.identity).name = "Cube"+index;
            index++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //RotateItem();
        allItem = GameObject.FindGameObjectsWithTag("Wall");
        if (allItem.Length < 5) 
        {
            Vector3 randSpawn = new Vector3(
               Random.Range(MinRandPos.x, MaxRandPos.x),
               0.5f,
               Random.Range(MinRandPos.z, MaxRandPos.z));
             Instantiate(ItemPrefab, randSpawn, Quaternion.identity).name = "Cube";
        }
    }
    void SpawnItem()
    {

    }
    void RotateItem()
    {
         allItem = GameObject.FindGameObjectsWithTag("Item");
        foreach(var item in allItem)
        {
            item.transform.Rotate(new Vector3(50, 50, 50) * 2.0f * Time.deltaTime, Space.Self);
        }
    }
   
}
