using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainSceneCamera : MonoBehaviour
{
    GameObject Player;
    [SerializeField] public float x = 0f;
    [SerializeField] public float y = 2f;
    [SerializeField] public float z = -6f;
    // Start is called before the first frame update
    private void Awake()
    {
        
        //Player = Loading.Instance.CurrentPlayer;
    }
    void Start()
    {
        float posx = Player.transform.position.x;
        float posy = Player.transform.position.y;
        float posz = Player.transform.position.z;
        gameObject.transform.position = new Vector3(x + posx, y + posy, z + posz);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
