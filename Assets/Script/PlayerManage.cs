using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerManage : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject _playerPrefs;
    [SerializeField] public GameObject _textMeshPro;
    string userName = "";
    private Button ButtonBack;
    private void Awake()
    {
        //_playerPrefs = ObjecTable.InstanceModel();
        _playerPrefs.transform.position = new Vector3(0.4324223f, 8.5f, 0.1471925f);
        _playerPrefs.transform.rotation = new Quaternion(0, 180, 0, 0);
    }
    private void OnEnable()
    {
        ButtonBack = GameObject.Find("ButtonBack").GetComponent<Button>();
        _textMeshPro.GetComponent<TextMeshProUGUI>().text = userName;
    }
    // Start is called before the first frame update
    void Start()
    {
        ButtonBack.onClick.AddListener(ChangeScene);
    }

    // Update is called once per frame
    void Update()
    {

    }
    void ChangeScene()
    {
        Destroy(gameObject);
        SceneManager.LoadScene("LoadingScene");
    }
}
