using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider HealthSlide;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetHealth(float health)
    {
        HealthSlide.value -= health;
    }
    public void InCreaseHealth()
    {
        HealthSlide.value += 3;
    }
    public void SetMaxHealth(float health)
    {
        HealthSlide.maxValue = health;
        HealthSlide.value = health;
    }
}
